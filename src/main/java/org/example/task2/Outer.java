package org.example.task2;

public class Outer {
    private static class Inner{
        private static int count;

        public Inner() {
            System.out.println("New Inner instance created");
            count++;
        }

        public static int getCount(){
            return count;
        }
    }

    public static void main(String[] args) {
        Outer.Inner inner = new Outer.Inner();// Inner inner = new Inner() is also true
        System.out.println("The number of instances: " + Inner.getCount());

        Outer.Inner inner1 = new Outer.Inner();// Inner inner1 = new Inner() is also true
        System.out.println("The number of instances: " + Inner.getCount());

        Outer.Inner inner2 = new Outer.Inner();// Inner inner2 = new Inner() is also true
        System.out.println("The number of instances: " + Inner.getCount());
    }
}
