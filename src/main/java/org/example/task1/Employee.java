package org.example.task1;

public interface Employee {

    double calculateSalary();

    void displayDetails();

}
