package org.example.task1;

public class PartTimeEmployee implements Employee{
    private String name;
    private int id;
    private double hourlyRate;
    private double hoursWorked;

    public PartTimeEmployee(String name, int id, double hourlyRate, double hoursWorked) {
        setName(name);
        setId(id);
        setHourlyRate(hourlyRate);
        setHoursWorked(hoursWorked);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public double getHoursWorked() {
        return hoursWorked;
    }

    public void setHoursWorked(double hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    @Override
    public double calculateSalary() {
        return hourlyRate * hoursWorked;
    }

    @Override
    public void displayDetails() {
        System.out.println("Id: " + this.getId());
        System.out.println("Name: " + this.getName());
        System.out.println("Employment Status: Part-Time Employee");
        System.out.println("Hourly Rate: " + this.getHourlyRate() +" USD ");
        System.out.println("Hours Worked: " + this.getHoursWorked());
    }
}
