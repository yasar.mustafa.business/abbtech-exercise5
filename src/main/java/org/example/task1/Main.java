package org.example.task1;

public class Main {
    public static void main(String[] args) {

        Employee[] employees = {
                new FullTimeEmployee("John Stones", 101, 5000),
                new FullTimeEmployee("Jane Doyle", 102, 6000),
                new FullTimeEmployee("Bob Parker", 103, 5500),
                new PartTimeEmployee("Alice Brown", 201, 20, 25),
                new PartTimeEmployee("Charlie Nelson", 202, 18, 30.5),
                new PartTimeEmployee("Thomas Martinez", 203, 22, 20)
        };


        for(Employee e : employees){
            e.displayDetails();
            System.out.println("Salary: " + e.calculateSalary() + " USD ");
            System.out.println();
        }
    }
}